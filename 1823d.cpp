#include <bits/stdc++.h>
using namespace std;

#define int long long

int was[30][30];

void solve() {
    memset(was, 0, sizeof(was));
    int n, k;
    cin >> n >> k;
    vector<int> x(k), c(n + 1);
    vector<int> checkpoints(n + 1);
    for (int i = 0; i < k; i++) {
        cin >> x[i];
        checkpoints[x[i]] = 1;
    }
    for (int i = 0; i < k; i++) {
        int a;
        cin >> a;
        c[x[i]] = a;
    }
    vector<int> needs(n + 1, 0);
    for (int i = 1; i <= n; i++) {
        needs[i] = c[*(lower_bound(x.begin(), x.end(), i))];
    }
    string ans;
    int aux = 0;
    int tot = 0;
    int memo = -1;
    for (int i = 1; i <= n; i++) {
        if (tot < needs[i]) {
            if (aux < 6) {
                ans += 'a' + aux++;
                tot++;
            } else {
                memo = ans[i - 2];
                while (i <= n and tot < needs[i]) {
                    ans += 'a' + aux;
                    tot++;
                    if (checkpoints[i] and tot != needs[i]) {
                        cout << "NO\n";
                        return;
                    }
                    i++;
                }
                aux++;
                i--;
                continue;
            }
        } else if (tot == needs[i]) {
            if (aux < 3) {
                cout << "NO\n";
            }
            int now = (ans[i - 3] == 'a');
            if (memo != -1) now = (memo + 1) % 3;
            while (i <= n and tot == needs[i]) {
                ans += 'a' + (now++) % 3;
                i++;
            }
            i--;
            continue;
        } else if (tot > needs[i]) {
            cout << "NO\n";
            return;
        }
        if (checkpoints[i] and tot != needs[i]) {
            cout << "NO\n";
            return;
        }
    }

    cout << "YES\n";
    cout << ans << "\n";
}

signed main() {
    int ct;
    cin >> ct;
    while (ct--) solve();
}
