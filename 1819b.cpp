#include <bits/stdc++.h>
using namespace std;

#define int long long
typedef pair<int, int> ii;
#define f first
#define s second

int n;
vector<ii> ans;
vector<ii> pieces;

set<ii> H, W;
map<int, ii> m;

void solve() {
    int area = 0;
    ans.clear();
    H.clear();
    W.clear();
    m.clear();
    for (int i = 0; i < n; i++) {
        H.insert({pieces[i].f, i});
        W.insert({pieces[i].s, i});
        m[i] = {pieces[i].f, pieces[i].s};
        area += pieces[i].f * pieces[i].s;
    }
    if (area % H.rbegin()->f == 0) {
        int cnt = 1, h = H.rbegin()->f, w = area / H.rbegin()->f;
        ans.push_back(ii(h, w));
        w -= m[H.rbegin()->s].s;
        W.erase({m[H.rbegin()->s].s, H.rbegin()->s});
        H.erase({H.rbegin()->f, H.rbegin()->s});
        for (int i = 1; i < n; i++) {
            if (H.rbegin()->f == h) {
                cnt++;
                w -= m[H.rbegin()->s].s;
                W.erase({m[H.rbegin()->s].s, H.rbegin()->s});
                H.erase({H.rbegin()->f, H.rbegin()->s});
            } else if (W.rbegin()->f == w) {
                cnt++;
                h -= m[W.rbegin()->s].f;
                H.erase({m[W.rbegin()->s].f, W.rbegin()->s});
                W.erase({W.rbegin()->f, W.rbegin()->s});
            } else
                break;
        }
        if (cnt < n) {
            ans.pop_back();
        }
    }
    H.clear();
    W.clear();
    for (int i = 0; i < n; i++) {
        H.insert({pieces[i].f, i});
        W.insert({pieces[i].s, i});
    }
    if (area % W.rbegin()->f == 0) {
        int cnt = 1, w = W.rbegin()->f, h = area / W.rbegin()->f;
        ans.push_back(ii(h, w));
        h -= m[W.rbegin()->s].f;
        H.erase({m[W.rbegin()->s].f, W.rbegin()->s});
        W.erase({W.rbegin()->f, W.rbegin()->s});
        for (int i = 1; i < n; i++) {
            if (W.rbegin()->f == w) {
                cnt++;
                h -= m[W.rbegin()->s].f;
                H.erase({m[W.rbegin()->s].f, W.rbegin()->s});
                W.erase({W.rbegin()->f, W.rbegin()->s});
            } else if (H.rbegin()->f == h) {
                cnt++;
                w -= m[H.rbegin()->s].s;
                W.erase({m[H.rbegin()->s].s, H.rbegin()->s});
                H.erase({H.rbegin()->f, H.rbegin()->s});
            } else
                break;
        }
        if (cnt < n) {
            ans.pop_back();
        }
    }
    if (ans.size() == 2) {
        if (ans[0] == ans[1]) {
            ans.pop_back();
        }
    }
    cout << ans.size() << endl;
    for (int i = 0; i < ans.size(); i++) {
        cout << ans[i].f << " " << ans[i].s << endl;
    }
}

signed main() {
    int ct;
    cin >> ct;
    while (ct--) {
        cin >> n;
        pieces.clear();
        pieces.resize(n);
        for (int i = 0; i < n; i++) {
            cin >> pieces[i].f >> pieces[i].s;
        }
        solve();
    }
}
