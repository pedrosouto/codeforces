#include <bits/stdc++.h>
using namespace std;

#define int long long
#define IOS                           \
    ios_base::sync_with_stdio(false); \
    cin.tie(0);                       \
    cout.tie(0);

void solve() {
    int n;
    cin >> n;
    vector<int> v, w;
    vector<int> mp;
    for (int i = 0; i < n; i++) {
        string s;
        cin >> s;
        int now = 0;
        int add = 0;
        for (auto c : s) {
            now ^= (1ll << (int)(c - 'a'));
            add |= (1ll << (int)(c - 'a'));
        }
        v.push_back(now);
        w.push_back(add);
        mp.push_back(now);
    }
    sort(mp.begin(), mp.end());
    int ans = 0;
    for (int j = 0; j < 26; j++) {  // bruteforcing the missing letter
        vector<int> tmp;
        for (int i = 0; i < n; i++) {
            if (w[i] & (1ll << j)) {
                tmp.push_back(v[i]);
            }
        }
        sort(tmp.begin(), tmp.end());
        for (int i = 0; i < n; i++) {
            if (w[i] & (1ll << j)) continue;
            int goal = (1ll << 26) - 1;
            goal ^= (1ll << j);
            goal ^= v[i];
            int count1 = upper_bound(tmp.begin(), tmp.end(), goal) - lower_bound(tmp.begin(), tmp.end(), goal);
            int count2 = upper_bound(mp.begin(), mp.end(), goal) - lower_bound(mp.begin(), mp.end(), goal);
            ans += count2 - count1;
        }
    }
    cout << ans / 2 << "\n";
}

signed main() {
    solve();
}
