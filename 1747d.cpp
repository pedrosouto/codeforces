#include <bits/stdc++.h>
using namespace std;
#define int long long
#define IOS                       \
    ios_base::sync_with_stdio(0); \
    cin.tie(0)
/* start */

signed main() {
    IOS;
    int n, q;
    cin >> n >> q;
    vector<int> a(n + 1);
    for (int i = 1; i <= n; i++) cin >> a[i];
    vector<int> p(n + 1);
    vector<int> sum(n + 1);
    for (int i = 1; i <= n; i++) sum[i] += sum[i - 1] + a[i];
    p[0] = a[0];
    map<int, vector<int>> mi;
    map<int, vector<int>> mp;
    mp[0].push_back(0);
    for (int i = 1; i <= n; i++) {
        p[i] = p[i - 1] ^ a[i];
        if (i & 1)
            mi[p[i]].push_back(i);
        else
            mp[p[i]].push_back(i);
    }
    while (q--) {
        int l, r;
        cin >> l >> r;
        int size = r - l + 1;
        // cout << size << " ";
        if (sum[r] - sum[l - 1] == 0) {
            cout << "0\n";
            continue;
        }
        if (size & 1) {
            if (size == 1) {
                if (a[l]) {
                    cout << "-1\n";
                } else
                    cout << "0\n";
            } else if (p[l - 1] == p[r]) {
                cout << "1\n";
            } else
                cout << "-1\n";
            continue;
        }
        if ((p[r] ^ p[l - 1]) != 0) {
            cout << "-1\n";
            continue;
        }
        if (size == 2) {
            if ((a[l] or a[r])) {
                cout << "-1\n";
            } else
                cout << "0\n";
            continue;
        }
        if (a[l] == 0 or a[r] == 0) {
            cout << "1\n";
            continue;
        }
        if (l & 1) {
            auto aux = upper_bound(mi[p[l - 1]].begin(), mi[p[l - 1]].end(), l);
            if (aux == mi[p[l - 1]].end()) {
                cout << "-1\n";
            } else {
                if (*aux < r) {
                    cout << "2\n";
                } else
                    cout << "-1\n";
            }
        } else {
            auto aux = upper_bound(mp[p[l - 1]].begin(), mp[p[l - 1]].end(), l);
            if (aux == mp[p[l - 1]].end()) {
                cout << "-1\n";
            } else {
                if (*aux < r) {
                    cout << "2\n";
                } else
                    cout << "-1\n";
            }
        }
    }
}
