#include <bits/stdc++.h>
using namespace std;

#define int long long

void solve() {
    int n;
    cin >> n;
    vector<int> used(n + 1, 0);
    vector<int> ans(n);
    vector<int> b(n);
    for (int i = 0; i < n / 2; i++) {
        int x;
        cin >> x;
        used[x]++;
        ans[2 * i + 1] = x;
        b[i] = x;
    }
    set<int> nused;
    for (int i = 1; i <= n; i++)
        if (!used[i]) nused.insert(-i);
    if (nused.size() != n / 2) {
        cout << "-1\n";
        return;
    }
    for (int i = n / 2 - 1; i >= 0; i--) {
        auto it = nused.upper_bound(-b[i]);
        if (it == nused.end()) {
            cout << "-1\n";
            return;
        }
        ans[2 * i] = -(*it);
        nused.erase(it);
    }
    for (auto x : ans) cout << x << " ";
    cout << "\n";
}

signed main() {
    int ct;
    cin >> ct;
    while (ct--) solve();
}
