#include <bits/stdc++.h>
using namespace std;

#define int long long

void solve() {
    string l, r, ans;
    cin >> l >> r;
    if (l.size() < r.size()) {
        for (int i = 0; i < l.size(); i++) cout << "9";
        cout << "\n";
        return;
    }
    int bst = 10;
    for (int a = 0; a < 10; a++) {
        for (int b = 0; b < 10; b++) {
            if (a > b) continue;
            int flag = 0;
            vector<int> tmp;
            for (int i = 0; i < l.size(); i++)
                tmp.push_back(l[i] - '0');

            int cringe = 0;
            for (int i = 0; i < tmp.size() and !cringe; i++) {
                while (tmp[i] < a or tmp[i] > b) {
                    if (!flag) {
                        for (int j = i + 1; j < tmp.size(); j++) {
                            tmp[j] = 0;
                        }
                        flag = 1;
                    }
                    tmp[i]++;
                    for (; i >= 0 and tmp[i] == 10; i--) {
                        tmp[i] = 0;
                        if (i - 1 < 0) {
                            cringe = 1;
                            break;
                        } else
                            tmp[i - 1]++;
                    }
                }
            }
            if (cringe) continue;
            string res = "";
            for (int i = 0; i < tmp.size(); i++)
                res += (char)(tmp[i] + '0');

            for (int i = 0; i < res.size(); i++) {
                if (res[i] > r[i]) {
                    cringe = 1;
                    break;
                }
                if (res[i] < r[i]) break;
            }
            if (cringe) continue;
            if (b - a < bst) {
                bst = b - a;
                ans = res;
            }
        }
    }
    cout << ans << "\n";
}

signed main() {
    int ct;
    cin >> ct;
    while (ct--) solve();
}
