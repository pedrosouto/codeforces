#include <bits/stdc++.h>
using namespace std;
#define int long long
#define IOS                       \
    ios_base::sync_with_stdio(0); \
    cin.tie(0)
/* start */

void solve() {
    int n;
    cin >> n;
    vector<int> a(n);
    vector<int> q(n);
    vector<int> ra(n);
    for (int i = 0; i < n; i++) {
        cin >> a[i];
        ra[a[i] - 1] = i + 1;
        q[a[i] - 1] = i + 1;
    }
    if (n % 2 == 0) swap(q[n / 2 - 1], q[0]);
    swap(q[n / 2], q[n - 1]);
    for (int i = 0; i < n / 2; i++) {
        swap(q[i], q[n - i - 1]);
    }
    for (int i = 0; i < n; i++) {
        if (q[i] == i + 1) {
            if (i < n - 1) swap(q[i], q[i + 1]);
            if (i == n - 1 and n > 1) {
                swap(q[n - 2], q[n - 1]);
                if (q[n - 1] == ra[n - 1]) {
                    for (int j = 0; j < n; j++) {
                        if (q[j] == n) continue;
                        swap(q[j], q[n - 1]);
                        break;
                    }
                }
            }
        }
    }

    vector<int> p(n);
    for (int i = 0; i < n; i++) {
        p[q[i] - 1] = ra[i];
    }

    // for(auto x : q) cout << x << " ";
    // cout << "\n";
    // for(auto x : p) cout << x << " ";
    // cout << "\n";

    for (int i = 0; i < n; i++) {
        if (q[i] == i + 1 or p[i] == i + 1) {
            cout << "Impossible\n";
            return;
        }
    }

    cout << "Possible\n";
    for (auto x : p) cout << x << " ";
    cout << "\n";
    for (auto x : q) cout << x << " ";
    cout << "\n";
}

signed main() {
    IOS;
    int ct;
    cin >> ct;
    while (ct--) solve();
}
