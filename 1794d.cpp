#include <bits/stdc++.h>
using namespace std;

#define int long long
#define ios                           \
    ios_base::sync_with_stdio(false); \
    cin.tie(0);                       \
    cout.tie(0);

const int N = 1e6 + 5;
const int mod = 998244353;

int fat[N], inv[N];
int dp[5000][5000];

int pow(int a, int b) {
    if (b == 0) return 1;
    int ans = pow(a, b / 2);
    ans = (ans * ans) % mod;
    if (b % 2) ans = (ans * a) % mod;
    return ans;
}

void precalc() {
    fat[0] = 1;
    for (int i = 1; i < N; i++) fat[i] = (i * fat[i - 1]) % mod;
    inv[0] = 1;
    for (int i = 0; i < N; i++) inv[i] = pow(fat[i], mod - 2);
}

bool is_prime(int n) {
    if (n < 2) return false;
    for (int i = 2; i * i <= n; i++) {
        if (n % i == 0) return false;
    }
    return true;
}

void solve() {
    int n;
    cin >> n;
    vector<int> a(2 * n), count(N, 0), primes, unwanted;
    for (auto &x : a) {
        cin >> x;
        count[x]++;
        if (is_prime(x) and count[x] == 1) primes.push_back(x);
        if (!is_prime(x) and count[x] == 1) unwanted.push_back(x);
    }
    int k = fat[n];
    for (auto x : unwanted) k = (k * inv[count[x]]) % mod;
    dp[0][0] = k;
    for (int i = 1; i <= primes.size(); i++) {
        for (int j = 0; j <= n; j++) {
            dp[i][j] = (dp[i][j] + (dp[i - 1][j] * inv[count[primes[i - 1]]]) % mod) % mod;
            if (j) dp[i][j] = (dp[i][j] + (dp[i - 1][j - 1] * inv[count[primes[i - 1]] - 1]) % mod) % mod;
        }
    }
    if (primes.size() == 0)
        cout << 0 << "\n";
    else
        cout << dp[primes.size()][n] << "\n";
}

signed main() {
    ios;
    precalc();
    solve();
}
