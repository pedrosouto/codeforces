#include <bits/stdc++.h>
using namespace std;

#define int long long
#define ios                           \
    ios_base::sync_with_stdio(false); \
    cin.tie(0);                       \
    cout.tie(0);

typedef pair<int, int> pii;

const int N = 2e5 + 5;
int ct = 1;
int n, cy_n;
int a[N], c[N], vis[N], dad[N], sz[N];
vector<int> g[N], g1[N];

int find(int x) {
    return x == dad[x] ? x : dad[x] = find(dad[x]);
}

void join(int x, int y) {
    x = find(x);
    y = find(y);
    if (x == y) return;
    if (sz[x] < sz[y]) swap(x, y);
    dad[y] = x;
    sz[x] += sz[y];
}

void dfs(int u) {
    for (auto v : g[u]) {
        if (vis[v]) {
            cy_n += abs(vis[u] - vis[v]) + 1;
            continue;
        }
        vis[v] = vis[u] + 1;
        dfs(v);
    }
}

int dfs2(int u) {
    c[u] = 0;
    for (auto v : g1[u]) {
        if (vis[v]) {
            c[u] += abs(vis[u] - vis[v]) + 1;
            continue;
        }
        vis[v] = vis[u] + 1;
        c[u] += dfs2(v);
    }
    return c[u];
}

void solve() {
    cin >> n;
    for (int i = 1; i <= n; i++) g[i].clear(), g1[i].clear();
    for (int i = 1; i <= n; i++) dad[i] = i, sz[i] = 1;
    for (int i = 1; i <= n; i++) {
        cin >> a[i];
        if (a[i] + i <= 0 || a[i] + i > n) continue;
        g[i].push_back(a[i] + i);
        g1[a[i] + i].push_back(i);
        join(a[i] + i, i);
    }
    memset(vis, 0, sizeof(vis));
    memset(c, 0, sizeof(c));
    cy_n = 0;
    for (int i = 1; i <= n; i++) {
        if (vis[i]) continue;
        vis[i] = 1;
        dfs(i);
    }
    memset(vis, 0, sizeof(vis));
    int ans = 0;
    for (int i = 1; i <= n; i++) {
        if (!vis[i]) {
            int k = find(i);
            vis[k] = 1;
            dfs2(k);
        }
        int res;
        if (find(i) == find(1))
            res = 2 * n + 1 - sz[find(i)] - cy_n + c[i];
        else
            res = 2 * n + 1;
        ans += res;
    }
    cout << "Case #" << ct << ": \n";
    cout << "cy_n = " << cy_n << "\n";
    cout << "w[i] = "
         << "\n";
    for (int i = 1; i <= n; i++) cout << sz[find(i)] << " ";
    cout << "\n";
    cout << "c[i] = "
         << "\n";
    for (int i = 1; i <= n; i++) cout << c[i] << " ";
    cout << "\n";
    cout << ans << "\n";
}

signed main() {
    cin >> ct;
    while (ct--) solve();
}
