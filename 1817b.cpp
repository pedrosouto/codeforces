#include <bits/stdc++.h>
using namespace std;

#define int long long
typedef pair<int, int> ii;

const int N = 2e3 + 5;
vector<int> g[N];
vector<int> vis, connected, dad;
int n, m, start, z;

bool dfs(int v, int p = -1) {
    vis[v] = 1;
    if (connected[v] && p != start) {
        z = v;
        return true;
    }
    for (auto x : g[v]) {
        if (vis[x]) continue;
        dad[x] = v;
        if (dfs(x, v)) return true;
    }
    return false;
}

void solve() {
    cin >> n >> m;
    for (int i = 1; i <= n; i++) g[i].clear();
    for (int i = 1; i <= m; i++) {
        int u, v;
        cin >> u >> v;
        g[u].push_back(v);
        g[v].push_back(u);
    }
    for (int i = 1; i <= n; i++) {
        if (g[i].size() < 4) continue;
        vis.assign(n + 1, 0);
        connected.assign(n + 1, 0);
        dad.assign(n + 1, 0);
        start = i;
        z = -1;
        for (auto x : g[i]) connected[x] = 1;
        if (!dfs(i)) continue;
        vector<ii> ans;
        ans.push_back({i, z});
        int zz = -1;
        for (int j = z; j != i; j = dad[j]) {
            if (dad[j] == i) zz = j;
            ans.push_back({j, dad[j]});
        }
        int aux = 0;
        for (auto x : g[i]) {
            if (x == z || x == zz) continue;
            if (aux == 2) break;
            ans.push_back({i, x});
            aux++;
        }
        cout << "YES\n";
        cout << ans.size() << "\n";
        for (auto x : ans) cout << x.first << " " << x.second << "\n";
        return;
    }
    cout << "NO\n";
}

signed main() {
    int ct;
    cin >> ct;
    while (ct--) solve();
}
