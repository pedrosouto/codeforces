mod = 998244353
N = 312345


def fexp(b, e):
    if e == 0:
        return 1
    a = fexp(b, e//2)
    a = (a*a) % mod
    if e & 1:
        a = (a*b) % mod
    return a


n, m = (int(x) for x in input().split())
crivo = [0]*N
primes = []
for i in range(2, N):
    if crivo[i]:
        continue
    primes.append(i)
    for j in range(2, N):
        if i*j >= N:
            break
        crivo[i*j] = 1

ans = 0

for i in range(1, n+1):
    ans = ans + fexp(m, i)

v = [0]*N
was = [0]*N
k = 1
v[0] = 1
for i in range(1, n+1):
    now = 1
    for j in range(0, 20):
        p = primes[j]
        if i % p == 0 and not was[p]:
            now *= p
            was[p] += 1
    k = k*now
    v[i] = ((m//k)*v[i-1]) % mod
    ans = (ans - v[i] + mod) % mod
    if not v[i]:
        break

print(ans)
