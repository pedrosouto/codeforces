#include <bits/stdc++.h>
using namespace std;

#define int long long
#define f first
#define s second
#define IOS                  \
    ios::sync_with_stdio(0); \
    cin.tie(0);              \
    cout.tie(0)

void solve() {
    int n;
    cin >> n;
    vector<vector<int> > a;
    for (int i = 0; i < n; i++) {
        int k = 0;
        cin >> k;
        vector<int> tmp;
        for (int j = 0; j < k; j++) {
            int x;
            cin >> x;
            if (tmp.size() == 0)
                tmp.push_back(x);
            else if (tmp.back() < x)
                tmp.push_back(x);
        }
        a.push_back(tmp);
    }
    map<int, vector<int> > mp;
    for (int i = 0; i < n; i++) {
        for (auto j : a[i]) {
            mp[j].push_back(i);
        }
    }
    int cur = 0;
    vector<int> dp(n, 0);
    for (auto i : mp) {
        int now = i.f;
        vector<int> al = i.s;
        int newcur = 0;
        for (auto j : al) {
            if (a[j].back() == now) {
                dp[j] = max(dp[j] + 1, 1 + cur);
                newcur = max(newcur, dp[j]);
                continue;
            }
            if (a[j].front() == now) {
                dp[j] = 1 + cur;
                continue;
            }
            dp[j] = max(dp[j] + 1, cur + 1);
        }
        cur = max(cur, newcur);
    }
    cout << *max_element(dp.begin(), dp.end()) << endl;
}

signed main() {
    IOS;
    int ct;
    cin >> ct;
    while (ct--) solve();
}
