#include <bits/stdc++.h>
using namespace std;
#define int long long
#define IOS                       \
    ios_base::sync_with_stdio(0); \
    cin.tie(0)
/* start */

#define N 105

void solve() {
    int n, p;
    cin >> n >> p;
    vector<int> a(n);
    for (auto &x : a) cin >> x;
    int k = a[n - 1];
    int l = 0, r = p;
    while (r - l > 0) {
        int m = (l + r) / 2;
        int count = m;
        set<int> foi;
        if (k + m >= p) {
            int flag = 1;
            for (int i = n - 2; i >= 0; i--) {
                if (a[i] + 1 < p) {
                    foi.insert(a[i] + 1);
                    flag = 0;
                    break;
                }
            }
            if (flag) foi.insert(1);
        }
        for (auto x : a) foi.insert(x);

        for (auto x : foi) {
            int cur = x;
            if (cur < k) cur += p;
            if (cur == k or k + m < cur) count++;
        }

        if (count < p)
            l = m + 1;
        else
            r = m;
    }
    cout << l << "\n";
}

signed main() {
    IOS;
    int ct;
    cin >> ct;
    while (ct--) solve();
}
