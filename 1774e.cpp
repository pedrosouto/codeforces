#include <bits/stdc++.h>
using namespace std;
#define int long long
#define IOS                       \
    ios_base::sync_with_stdio(0); \
    cin.tie(0)
/* start */

typedef pair<int, int> ii;

const int N = 2e5 + 9;

int n, d;
int pai[N];
int h[N];
int check[N];
int ans = 0;

vector<int> g[N];

void dfs(int x) {
    for (auto y : g[x]) {
        if (y == pai[x]) continue;
        pai[y] = x;
        h[y] = h[x] + 1;
        dfs(y);
    }
}

void goup(int x, int y) {
    if (pai[x] == 0 or check[x] & y) return;
    check[x] += y;
    ans += 2;
    goup(pai[x], y);
}

void goup2(int x, int y) {
    if (h[x] - d < 0) return;
    int dist = 0;
    while (!(check[x] & y) and dist < d) {
        x = pai[x];
        dist++;
    }
    if (dist == d) goup(x, y);
}

signed main() {
    IOS;
    cin >> n >> d;
    for (int i = 0; i < n - 1; i++) {
        int x, y;
        cin >> x >> y;
        g[x].push_back(y);
        g[y].push_back(x);
    }
    dfs(1);
    int k;
    cin >> k;
    vector<ii> a;
    for (int i = 0; i < k; i++) {
        int now;
        cin >> now;
        goup(now, 1);
        a.push_back(ii(-h[now], now));
    }
    cin >> k;
    vector<ii> b;
    for (int i = 0; i < k; i++) {
        int now;
        cin >> now;
        goup(now, 2);
        b.push_back(ii(-h[now], now));
    }
    sort(a.begin(), a.end());
    sort(b.begin(), b.end());
    for (auto x : a) {
        goup2(x.second, 2);
    }
    for (auto x : b) {
        goup2(x.second, 1);
    }
    cout << ans << "\n";
}
#include <bits/stdc++.h>
using namespace std;
#define int long long
#define IOS                       \
    ios_base::sync_with_stdio(0); \
    cin.tie(0)
/* start */

typedef pair<int, int> ii;

const int N = 2e5 + 9;

int n, d;
int pai[N];
int h[N];
int check[N];
int ans = 0;

vector<int> g[N];

void dfs(int x) {
    for (auto y : g[x]) {
        if (y == pai[x]) continue;
        pai[y] = x;
        h[y] = h[x] + 1;
        dfs(y);
    }
}

void goup(int x, int y) {
    if (pai[x] == 0 or check[x] & y) return;
    check[x] += y;
    ans += 2;
    goup(pai[x], y);
}

void goup2(int x, int y) {
    if (h[x] - d < 0) return;
    int dist = 0;
    while (!(check[x] & y) and dist < d) {
        x = pai[x];
        dist++;
    }
    if (dist == d) goup(x, y);
}

signed main() {
    IOS;
    cin >> n >> d;
    for (int i = 0; i < n - 1; i++) {
        int x, y;
        cin >> x >> y;
        g[x].push_back(y);
        g[y].push_back(x);
    }
    dfs(1);
    int k;
    cin >> k;
    vector<ii> a;
    for (int i = 0; i < k; i++) {
        int now;
        cin >> now;
        goup(now, 1);
        a.push_back(ii(-h[now], now));
    }
    cin >> k;
    vector<ii> b;
    for (int i = 0; i < k; i++) {
        int now;
        cin >> now;
        goup(now, 2);
        b.push_back(ii(-h[now], now));
    }
    sort(a.begin(), a.end());
    sort(b.begin(), b.end());
    for (auto x : a) {
        goup2(x.second, 2);
    }
    for (auto x : b) {
        goup2(x.second, 1);
    }
    cout << ans << "\n";
}
