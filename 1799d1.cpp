#include <bits/stdc++.h>
using namespace std;

#define int long long
#define IOS                      \
    ios::sync_with_stdio(false); \
    cin.tie(0);                  \
    cout.tie(0);

const int N = 5010;
int n, k;
int a[N], cold[N], hot[N], dp[N][N];

void solve() {
    cin >> n >> k;
    for (int i = 0; i <= n; i++) {
        for (int j = 0; j <= k; j++) {
            dp[i][j] = 1e18;
        }
    }
    for (int i = 1; i <= n; i++) cin >> a[i];
    for (int i = 1; i <= k; i++) cin >> cold[i];
    for (int i = 1; i <= k; i++) cin >> hot[i];
    a[0] = 0;
    dp[0][0] = 0;
    for (int i = 1; i <= n; i++) {
        for (int j = 0; j <= k; j++) {
            if (dp[i - 1][j] == 1e18) continue;
            int cp1 = a[i - 1], cp2 = j;
            //            cout << i << " " << j << " " << cp1 << " " << cp2 << "\n";
            if (cp1 == 0 && cp2 == 0) {
                dp[i][0] = min(dp[i][0], dp[i - 1][j] + cold[a[i]]);
                //               cout << dp[i][0] << "\n";
                continue;
            }
            if (cp1 == a[i]) {
                dp[i][cp2] = min(dp[i][cp2], dp[i - 1][j] + hot[a[i]]);
                //                cout << dp[i][cp2] << "\n";
                continue;
            }
            if (cp2 == 0) {
                dp[i][cp1] = min(dp[i][cp1], dp[i - 1][j] + cold[a[i]]);
                //                cout << dp[i][cp1] << "\n";
                continue;
            }
            if (cp2 == a[i]) {
                dp[i][cp1] = min(dp[i][cp1], dp[i - 1][j] + hot[a[i]]);
                //                cout << dp[i][cp1] << "\n";
                continue;
            } else {
                dp[i][cp1] = min(dp[i][cp1], dp[i - 1][j] + cold[a[i]]);
                dp[i][cp2] = min(dp[i][cp2], dp[i - 1][j] + cold[a[i]]);
            }
        }
    }
    int ans = 1e18;
    for (int i = 0; i <= k; i++) ans = min(ans, dp[n][i]);
    cout << ans << "\n";
}

signed main() {
    IOS;
    int ct;
    cin >> ct;
    while (ct--) solve();
}
