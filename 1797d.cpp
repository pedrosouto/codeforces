#include <bits/stdc++.h>
using namespace std;

#define int long long
#define IOS ios::sync_with_stdio(false); cin.tie(nullptr); cout.tie(nullptr);

typedef pair<int, int> ii;

const int N = 1e5 + 9;
int n, q;
int a[N], f[N], sum[N], sz[N];
vector<int> gf[N];
set<ii> g[N];

int dfs(int u, int p = -1){
    sum[u] = a[u];
    sz[u] = 1;
    f[u] = p;
    for(auto v : gf[u]){
        if(v == p) continue;
        sum[u] += dfs(v, u);
        sz[u] += sz[v];
        g[u].insert({-sz[v], v});
    }
    return sum[u];
}

void solve(){
    cin >> n >> q;
    for(int i = 0; i < n; i++) cin >> a[i + 1];
    for(int i = 0; i < n - 1; i++){
        int x, y; cin >> x >> y;
        gf[x].push_back(y);
        gf[y].push_back(x);
    }
    dfs(1);
    while(q--){
        int t, x; cin >> t >> x;
        if(t == 1){
            cout << sum[x] << "\n";
            continue;
        }
        if(g[x].size() == 0) continue;
        int s = g[x].begin()->second;
        g[f[x]].erase({-sz[x], x});
        g[x].erase({-sz[s], s});
        sz[x] -= sz[s];
        sum[x] -= sum[s];
        g[s].insert({-sz[x], x});
        sz[s] += sz[x];
        sum[s] += sum[x];
        g[f[x]].insert({-sz[s], s});
        f[s] = f[x];
        f[x] = s;

    }
}

signed main(){
    IOS;
    int ct = 1;
    while(ct--) solve();
}
