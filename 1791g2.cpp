#include <bits/stdc++.h>
using namespace std;

#define int long long
#define ios ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
#define f first
#define s second

typedef pair<int, int> pii;

void solve() {
    int n, c;
    cin >> n >> c;
    vector<int> a(n+1);
    vector<pii> b(n+1);
    for(int i = 1; i <= n; i++) {
        cin >> a[i];
        b[i] = {(a[i] + min(i, n - i + 1)), i};
    }
    sort(b.begin() + 1, b.end());
    vector<int> pos(n + 1);
    for(int i = 1; i <= n; i++) pos[b[i].s] = i;
    vector<int> pref(n + 1);
    pref[0] = 0;
    for(int i = 1; i <= n; i++) pref[i] = pref[i - 1] + b[i].f;
    int ans = 0;
    for(int i = 1; i <= n; i++) {
        int cur = a[i] + i;
        if(cur > c) continue;
        int L = pos[i] - 1;
        if(pref[L] + cur >= c) {
            // binary search to find the largest prefix that is not greater than c - cur
            int l = 0, r = n;
            while(l < r) {
                int mid = (l + r + 1) / 2;
                if(pref[mid] + cur <= c) l = mid;
                else r = mid - 1;
            }
            ans = max(ans, l + 1);
        }
        else {
            cur -= b[pos[i]].f;
            int l = 0, r = n;
            while(l < r) {
                int mid = (l + r + 1) / 2;
                if(pref[mid] + cur <= c) l = mid;
                else r = mid - 1;
            }
            ans = max(ans, l);
        }
    }
    cout << ans << '\n';
}

signed main() {
    ios;
    int ct;
    cin >> ct;
    while(ct--) solve();
}
