#include <bits/stdc++.h>
using namespace std;
#define int long long
#define IOS                       \
    ios_base::sync_with_stdio(0); \
    cin.tie(0)
/* start */

signed main() {
    IOS;
    int n, m;
    cin >> n >> m;
    vector<int> v(n);
    int ans = 0;
    for (auto &x : v) {
        cin >> x;
        ans += x + 1;
    }
    sort(v.begin(), v.end());
    int l = 0, r = n - 1;
    int now = 0;
    for (; l < r;) {
        if (v[l] + v[r] > m)
            r--;
        else if (!now) {
            ans--;
            r--;
            now ^= 1;
        } else {
            ans--;
            l++;
            now ^= 1;
        }
    }
    cout << ans << "\n";
}
