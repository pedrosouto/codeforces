#include <bits/stdc++.h>
using namespace std;
#define int long long
#define IOS                       \
    ios_base::sync_with_stdio(0); \
    cin.tie(0)
/* start */

typedef pair<int, int> ii;

signed main() {
    IOS;
    int n, k;
    cin >> n >> k;

    if (k == 1) {
        if (n == 1) {
            cout << "YES\n0\n";
            return 0;
        }
        if (n == 2) {
            cout << "YES\n1\n1 2\n";
            return 0;
        }
        cout << "YES\n";
        cout << n << "\n";
        int x = 1, y = 2;
        for (int i = 0; i < n - 1; i++) {
            cout << x++ << " " << y++ << "\n";
        }
        cout << n << " 1\n";
        return 0;
    }

    if (n < k + 1) {
        cout << "NO\n";
        return 0;
    }

    vector<int> count(n + 1, 0);
    vector<ii> g;

    for (int i = 0; i < k - 1; i++) {
        g.push_back(ii(i + 1, k));
        count[i + 1]++;
        count[k]++;
    }

    g.push_back(ii(k + 1, k));

    for (int i = 2; i < k - 1; i++) {
        for (int j = i, now = k - 1; j > 1 and i < now; j--, now--) {
            g.push_back(ii(i, now));
            count[i]++;
            count[now]++;
        }
    }

    for (int i = 2; i <= k - 1; i++) {
        if (count[i] == i) continue;
        g.push_back(ii(k + 1, i));
    }

    for (int i = k + 2, now = k + 1; i <= n; i++) {
        g.push_back(ii(now, i));
        now = i;
    }

    cout << "YES\n";
    cout << g.size() << "\n";
    for (auto x : g) {
        cout << x.first << " " << x.second << "\n";
    }
}
