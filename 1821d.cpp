#include <bits/stdc++.h>
using namespace std;

#define int long long

void solve() {
    int n, k;
    cin >> n >> k;
    vector<int> L(n), R(n);
    for (int i = 0; i < n; i++) cin >> L[i];
    for (int i = 0; i < n; i++) cin >> R[i];
    vector<int> sz(n, 0);
    for (int i = 0; i < n; i++) {
        sz[i] = R[i] - L[i] + 1;
    }

    int cnt = 0, sum = 0, ans = 1e18;
    for (int i = 0; i < n; i++) {
        if (sz[i] > 1)
            sum += sz[i];
        else
            cnt++;
        if (sum >= k) {
            ans = min(ans, 2 * (i + 1 - cnt) + R[i] - sum + k);
        } else if (sum + cnt >= k) {
            ans = min(ans, 2 * (i + 1 - cnt + k - sum) + R[i]);
        }
    }

    if (sum + cnt < k) {
        cout << "-1\n";
        return;
    }

    cout << ans << "\n";
}

signed main() {
    int ct;
    cin >> ct;
    while (ct--) solve();
}
