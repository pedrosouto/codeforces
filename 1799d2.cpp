#include <bits/stdc++.h>
using namespace std;

#define int long long
#define IOS                      \
    ios::sync_with_stdio(false); \
    cin.tie(0);                  \
    cout.tie(0);

void solve() {
    int n, k;
    cin >> n >> k;
    vector<int> a(n + 1), c(k + 1), h(k + 1);
    for (int i = 1; i <= n; i++) cin >> a[i];
    for (int i = 1; i <= k; i++) cin >> c[i];
    for (int i = 1; i <= k; i++) cin >> h[i];
    vector<int> pref(n + 1, 0);
    a[0] = 0;
    for (int i = 1; i <= n; i++) {
        if (a[i] == a[i - 1])
            pref[i] = pref[i - 1] + h[a[i]];
        else
            pref[i] = pref[i - 1] + c[a[i]];
    }
    vector<int> nxt(n + 1, -1), prevpos(k + 1, -1);
    for (int i = 1; i <= n; i++) {
        if (prevpos[a[i]] != -1) nxt[prevpos[a[i]]] = i;
        prevpos[a[i]] = i;
    }
    vector<int> A(n + 1, 1e18), B(n + 1, 1e18);
    A[1] = c[a[1]];
    for (int i = 1; i < n; i++) {
        if (a[i + 1] == a[i]) {
            B[i + 1] = min(B[i + 1], min(A[i], B[i]) + h[a[i + 1]]);
        }
        if (a[i + 1] != a[i]) {
            A[i + 1] = min(A[i + 1], min(A[i], B[i]) + c[a[i + 1]]);
        }
        if (nxt[i] == -1) continue;
        B[nxt[i]] = min(B[nxt[i]], B[i + 1] + pref[nxt[i] - 1] - pref[i + 1] + h[a[nxt[i]]]);
        B[nxt[i]] = min(B[nxt[i]], min(A[i], B[i]) + pref[nxt[i] - 1] - pref[i] + h[a[nxt[i]]]);
    }
    cout << min(A[n], B[n]) << "\n";
}

signed main() {
    IOS;
    int ct;
    cin >> ct;
    while (ct--) solve();
}
