#include <bits/stdc++.h>
using namespace std;

#define int long long

const int mod = 1e9 + 7;
const int N = 2e5 + 9;

vector<int> v[N];
int p2[N];

void prep() {
    p2[0] = 1;
    for (int i = 1; i < N; i++)
        p2[i] = (p2[i - 1] * 2) % mod;
}

void solve() {
    int n;
    cin >> n;
    vector<int> dad(n + 1, 0);
    vector<int> has(n + 1, 0);
    for (int i = 1; i <= n; i++) {
        v[i].clear();
    }
    for (int i = 0; i < n - 1; i++) {
        int x, y;
        cin >> x >> y;
        v[x].push_back(y);
        v[y].push_back(x);
    }
    queue<int> q;
    vector<int> level(n + 1, 0);
    vector<int> cnt(n + 1, 0);
    vector<int> h(n + 1, 0);
    q.push(1);
    h[1] = 1;

    while (!q.empty()) {
        int x = q.front();
        q.pop();
        for (auto y : v[x]) {
            if (h[y]) continue;
            q.push(y);
            dad[y] = x;
            has[x] = 1;
            h[y] = h[x] + 1;
        }
    }

    vector<pair<int, int> > nodes;
    for (int i = 1; i <= n; i++) {
        nodes.push_back(make_pair(-h[i], i));
    }
    sort(nodes.begin(), nodes.end());

    for (auto x : nodes) {
        int u = x.second;
        if (has[u]) continue;
        if (level[u]) continue;
        int now = u;
        level[u] = 1;
        cnt[level[u]]++;
        while (dad[now]) {
            if (level[dad[now]]) break;
            level[dad[now]] = level[now] + 1;
            now = dad[now];
            cnt[level[now]]++;
        }
    }
    int ans = 0;
    int aux = n;
    for (int i = 1; i <= n; i++) {
        if (cnt[i] == 0) break;
        ans = (ans + (p2[n - 1] * aux) % mod) % mod;
        aux -= cnt[i];
    }
    cout << ans << "\n";
}

signed main() {
    int ct;
    cin >> ct;
    prep();
    while (ct--) solve();
}
